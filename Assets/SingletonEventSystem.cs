﻿using UnityEngine;
using UnityEngine.EventSystems;

// ReSharper disable once CheckNamespace
public class SingletonEventSystem : MonoBehaviour
{
    private void Start()
    {
        var go = new GameObject(GetType().Name);
        go.AddComponent<EventSystem>();
        go.AddComponent<StandaloneInputModule>();
        DontDestroyOnLoad(go);
    }
}