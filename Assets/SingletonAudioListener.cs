﻿using UnityEngine;

// ReSharper disable once CheckNamespace
public class SingletonAudioListener : MonoBehaviour
{
    private void Start()
    {
        var go = new GameObject(GetType().Name);
        go.AddComponent<AudioListener>();
        DontDestroyOnLoad(go);
    }
}