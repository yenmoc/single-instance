# Single Instance

## Installation

```bash
"com.yenmoc.single-instance":"https://gitlab.com/yenmoc/single-instance"
or
npm publish --registry=http://localhost:4873
```

## Usages

* Add SingletonAudioListener to gameobject
* Add SingletonEventSystem to gameobject
